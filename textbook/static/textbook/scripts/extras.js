(function() {
  $(function() {
    $('aside.movie, nav').click(function(e) {
      var $this;
      if (e.target === this) {
        $this = $(this);
        $this.find('video, img').css('max-height', $(window).height() * .7);
        $this.toggleClass('open');
        return false;
      }
    });
    return $('nav > header').click(function(e) {
      return $(this).parent().click();
    });
  });

}).call(this);
