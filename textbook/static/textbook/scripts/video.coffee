metadata_loaded =  ->
    $container = $($(@).parent().get(0))
    @ratio = @videoHeight / @videoWidth
    $container.css('height', @ratio * $container.width())

enhance_video = ->
    if not videojs
        setTimeout enhance_video, 1000
    else
        videoItems = []
        $('video').each( ->
            $this = $(@)
            $this.addClass('video-js').addClass('vjs-default-skin')
            videoItems.push @
        )
        for item in videoItems
            videojs(item,
                controls: yes
                autoplay: no
                preload: 'auto'
                width: '100%'
                languages:
                    pl:
                        Play: 'Odtwarzaj'
                        Pause: 'Pauza'
                        Fullscreen: 'Cały ekran'
                        Subtitles: 'Napisy'
                        'subtitles off': 'bez napisów'
            )
        for item in videoItems
            item.onloadedmetadata = metadata_loaded
            item.load()


$ ->
    enhance_video()