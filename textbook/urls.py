from django.conf.urls import patterns, url
from django.views.generic import RedirectView

urlpatterns = patterns('textbook',
    url(r'^/$', 'views.index', name='textbook'),

    #url(r'^/(?P<volume>[^/]+)/(?P<type>[^/]+)/(?P<name>[^/]+)/?$', 'views.page'),
    url(r'^/(?P<volume_nr>\d+)/?$', 'views.page', name='volume'),
    url(r'^/(?P<volume_nr>\d+)/(?P<chapter_nr>\d+)/?$', 'views.page', name='chapter'),
    url(r'^/(?P<volume_nr>\d+)/(?P<chapter_nr>\d+)/(?P<subchapter_nr>\d+)/?$', 'views.page', name='subchapter'),
    url(r'^/images/(?P<file_path>.+)', 'views.image', name='image'),
    url(r'^/files/(?P<file_path>.+)', 'views.file', name='file'),
    url(r'^/exercises', RedirectView.as_view(url='zadania', permanent=True), name='exercises'),
)