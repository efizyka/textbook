from setuptools import setup

setup(
    name='textbook',
    version='0.1.1',
    description='textbook application for django website',
    url='https://bitbucket.org/efizyka/textbook',
    license='LICENSE',
    packages=[
        'textbook'
    ],
    install_requires=[
        'django-compressor',
        'coffee-compressor-compiler',
        'django-appsettings',
        'unipath',
        'lxml',
        'python-magic',
        'mock'
    ],
    zip_safe=False,
    include_package_data=True
)