$ ->
    #$('nav').nanoScroller({
    #    contentClass: 'nav-content'
    #})

    $('nav > ul > li > a')
    .data('parent', 'nav')
    .click( (evt) ->
        $this = $(this)
        $parent = $this.parent('li')
        if $parent.hasClass('active')
            evt.preventDefault()
            $('nav li:not(.active) .in').collapse('hide')
            $parent.find('ul').collapse('toggle')
        else
            if $parent.find('ul').hasClass('in')
                # go to the link
            else
                evt.preventDefault()
                $('nav .in').collapse('hide')
                $parent.find('ul').collapse('show')
    )

    $('nav > ul > li > ul > li > a').click((evt) ->
        if $(this).parent('li').hasClass('active')
            evt.preventDefault()
    )

    $('nav > ul > li > ul').addClass('collapse')
    $('nav li.active').closest('nav > ul > li').find('ul').collapse('show') #addClass('in')