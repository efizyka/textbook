$ ->
    rule = null
    setMaxWidth = ->
        windowWidth = $(window).width()
        console.log windowWidth
        $('body > header, body > footer, #content, #before, #after').css('maxWidth', windowWidth - 150)
        contentWidth = $('#content').outerWidth()
        console.log contentWidth
        maxMargin = - contentWidth / 2 - 70
        if rule
            rule.remove()
        rule = $("""<style type='text/css'>
            nav {
                margin-left: #{maxMargin}px !important;
            }
            aside.movie {
                margin-right: #{maxMargin}px !important;
            }
        </style>""")
        rule.appendTo('head');

    setMaxWidth()
    $( window ).bind('resize', setMaxWidth)
