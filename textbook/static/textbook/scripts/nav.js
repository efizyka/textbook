(function() {
  $(function() {
    $('nav > ul > li > a').data('parent', 'nav').click(function(evt) {
      $('nav .in').collapse('toggle');
      $($(this).attr('href')).collapse('toggle');
      return evt.preventDefault();
    });
    return $('nav > ul > li > ul').addClass('collapse');
  });

}).call(this);
