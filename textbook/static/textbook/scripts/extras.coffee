$ ->
    $('aside.movie, nav').click (e) ->
        if e.target == @
            $this = $(@)
            maxHeight = $(window).height() * .7
            $this.find('video, img').css('max-height', maxHeight)
            $this.find('video').parent('.video-js').css('max-height', maxHeight)
            $this.toggleClass 'open'
            $this.find('video').each( ->
                container = $(@).parent('.video-js').get(0)
                if container
                    $container = $(container)
                    $container.css('height', @ratio * maxHeight)
                    if $this.hasClass('open')
                        update = ->
                            $container.css('height', @ratio * $container.width())
                        setTimeout update, 300
            )
            no
    $('nav > header').click (e) ->
        $(@).parent().click()
