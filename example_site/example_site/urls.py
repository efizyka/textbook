from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^podrecznik', include('textbook.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^appsettings/', include('appsettings.urls')),
)
