(function() {
  $(function() {
    return $('.expert > header').addClass('collapsed').tooltip({
      placement: 'auto',
      title: 'Zobacz więcej'
    }).on('show.bs.tooltip', function() {
      return $(this).hasClass('collapsed');
    }).on('shown.bs.tooltip', function() {
      var $this;
      $this = $(this);
      return $this.next().css({
        'margin-left': -$this.width() / 2
      });
    });
  });

}).call(this);
