# -%- coding: utf-8 -%-

import re
import md5
from django.core.urlresolvers import reverse

from django.http import HttpResponse, Http404
from django.shortcuts import render, render_to_response
from django.core.cache import cache
from django.template import RequestContext
from django.views.decorators.cache import cache_page

from unipath import Path
from appsettings import app

import lxml.html
import magic

settings = app.settings.textbook


def index(request):
    return render_to_response('textbook/index.html')


def page(request, volume_nr, chapter_nr=1, subchapter_nr=None):
    volume_nr = int(volume_nr)
    volume_toc = __toc(volume_nr)
    chapter_nr = int(chapter_nr)
    if subchapter_nr:
        subchapter_nr = int(subchapter_nr)

    expanded, filename, target = __retrieve_target_file(chapter_nr, subchapter_nr, volume_toc)

    url_next, url_prev = __retrieve_nav_urls(chapter_nr, subchapter_nr, volume_nr, volume_toc)

    content = __read_content(filename)

    swap, swap_type, swap_label, template = __retrieve_swap(request)

    title, chapter_title, subchapter_title = __retrieve_titles(target)

    return __build_response(request, template, swap, {
        'content': content, 'toc': volume_toc, 'target': target,
        'volume_title': 'Tom ' + 'I'*volume_nr, 'expanded': expanded,
        'volume': volume_nr, 'chapter': chapter_nr, 'subchapter': subchapter_nr,
        'title': title, 'chapter_title': chapter_title, 'subchapter_title': subchapter_title,
        'url_prev': url_prev, 'url_next': url_next,
        'swap_type': swap_type, 'swap_label': swap_label,
        'current_path': request.path
    })


@cache_page(60 * 15)
def image(request, file_path):
    return __static_file(request, 'images', file_path)


@cache_page(60 * 15)
def file(request, file_path):
    return __static_file(request, 'files', file_path)


############## utils ###############

def cacheable(key_prefix):
    def decorator(function):
        def wrapper(*args, **kwargs):
            key_suffix = '%s:%s' % (str(args), str(kwargs))
            key = '%s:%s' % (key_prefix, md5.new(key_suffix).hexdigest())
            result = cache.get(key)
            if result is None:
                result = function(*args, **kwargs)
                cache.set(key, result)
            return result
        return wrapper
    return decorator


def __static_file(request, parent_dir, file_path):
    rootdir = Path(settings.root)
    filename = rootdir.child(parent_dir, *file_path.split('/'))
    if not filename.isfile():
        raise Http404
    mime = magic.from_file(filename.absolute(), mime=True)
    return HttpResponse(filename.read_file("rb"), mimetype=mime)


@cacheable('textbook_target')
def __retrieve_target_file(chapter_nr, subchapter_nr, volume_toc):
    if chapter_nr <= 0 or chapter_nr > len(volume_toc):
        raise Http404
    chapter = volume_toc[chapter_nr - 1]
    subchapters = chapter['children']
    expanded = [chapter]
    if subchapter_nr is not None:
        if subchapter_nr <= 0 or subchapter_nr > len(subchapters):
            raise Http404
        subchapter = subchapters[subchapter_nr - 1]
        target = subchapter
        expanded.append(subchapter)
    else:
        target = chapter
    filename = target['path']
    if not filename.isfile():
        raise Http404
    return expanded, filename, target


@cacheable('textbook_page_content')
def __read_content(filename):
    content = filename.read_file()
    content = re.sub('\\./images/([^\'"]+)', reverse('image', kwargs={'file_path': '11zz11'}).replace('11zz11', r'\1'), content)
    content = re.sub('\\./files/([^\'"]+)', reverse('file', kwargs={'file_path': '11zz11'}).replace('11zz11', r'\1'), content)
    content = re.sub('(?<=href=[\'"])(?!http)([^\'"#]+)', lambda m: __relative_path(filename, m.group(1)), content)
    return content


def __relative_path(path_from, path_to):
    filename = Path(path_from.parent + '/' + path_to).absolute()
    result = cache.get('textbook_url_for:%s' % str(filename))
    return result or path_to


@cacheable('textbook_toc')
def __toc(volume_nr):
    volumedir = Path(settings.root).child('tom%d' % volume_nr)
    print volumedir
    if not volumedir.isdir():
        raise Http404
    index = volumedir.child('index.html')
    if not index.isfile():
        raise Http404
    navs = index.read_file()
    nav = lxml.html.fromstring(navs)
    root = nav.xpath('//nav/ul')[0]
    result = []
    for chapter in root.xpath('li'):
        result.append(__tocitem(chapter, volumedir, None))
    for chapter in range(len(result)):
        cache.add('textbook_url_for:%s' % result[chapter]['path'], reverse('chapter', kwargs={'volume_nr': volume_nr, 'chapter_nr': chapter+1}))
        for subchapter in range(len(result[chapter]['children'])):
            cache.add('textbook_url_for:%s' % result[chapter]['children'][subchapter]['path'], reverse('subchapter', kwargs={'volume_nr': volume_nr, 'chapter_nr': chapter+1, 'subchapter_nr': subchapter+1}))
    return result


def __tocitem(item, volumedir, parent):
    result = {}
    a = item.xpath('a')[0]
    result['title'] = a.text_content()
    result['path'] = volumedir.child(*a.get('href').split('/'))
    result['parent'] = parent
    result['children'] = []
    for chapter in item.xpath('ul/li'):
        result['children'].append(__tocitem(chapter, volumedir, result))
    return result


def __retrieve_titles(target):
    if target['parent'] is None:
        chapter_title = target['title']
        subchapter_title = None
        title = chapter_title
    else:
        subchapter_title = target['title']
        chapter_title = target['parent']['title']
        title = subchapter_title
    return title, chapter_title, subchapter_title


@cacheable('textbook_nav_uls')
def __retrieve_nav_urls(chapter_nr, subchapter_nr, volume_nr, volume_toc):
    if subchapter_nr == 1:
        url_prev = reverse('chapter', kwargs={'volume_nr': volume_nr, 'chapter_nr': chapter_nr})
    elif subchapter_nr != None:
        url_prev = reverse('subchapter', kwargs={'volume_nr': volume_nr, 'chapter_nr': chapter_nr,
                                                 'subchapter_nr': subchapter_nr - 1})
    elif chapter_nr > 1:
        url_prev = reverse('subchapter', kwargs={'volume_nr': volume_nr, 'chapter_nr': chapter_nr - 1,                                                 'subchapter_nr': len(volume_toc[chapter_nr - 2]['children'])})
    else:
        url_prev = None

    if subchapter_nr == None:
        url_next = reverse('subchapter', kwargs={'volume_nr': volume_nr, 'chapter_nr': chapter_nr, 'subchapter_nr': 1})
    elif subchapter_nr < len(volume_toc[chapter_nr - 1]['children']):
        url_next = reverse('subchapter', kwargs={'volume_nr': volume_nr, 'chapter_nr': chapter_nr,
                                                 'subchapter_nr': subchapter_nr + 1})
    elif chapter_nr < len(volume_toc):
        url_next = reverse('chapter', kwargs={'volume_nr': volume_nr, 'chapter_nr': chapter_nr + 1})
    else:
        url_next = None

    return url_next, url_prev


def __retrieve_swap(request):
    swap = request.GET.get('type', '')
    if not swap:
        swap = request.COOKIES.get('textbook_swap', 'standard')
    if swap == 'standard':
        swap_type = 'accessible'
        swap_label = 'Wersja uproszczona'
        template = 'textbook/page_standard.html'
    else:
        swap_type = 'standard'
        swap_label = u'Wersja uproszczona włączona. Powrót do wersji standardowej.'
        template = 'textbook/page_accessible.html'
    return swap, swap_type, swap_label, template


# to cache or not to cache?
def __build_response(request, template, swap, context):
    response = render_to_response(template, RequestContext(request, context))
    response.set_cookie('textbook_swap', swap)
    return response
