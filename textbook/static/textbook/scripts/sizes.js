(function() {
  $(function() {
    var rule, setMaxWidth;
    rule = null;
    setMaxWidth = function() {
      var contentWidth, maxMargin, windowWidth;
      windowWidth = $(window).width();
      console.log(windowWidth);
      $('body > header, body > footer, #content').css('maxWidth', windowWidth - 150);
      contentWidth = $('#content').outerWidth();
      console.log(contentWidth);
      maxMargin = -contentWidth / 2 - 70;
      if (rule) {
        rule.remove();
      }
      rule = $("<style type='text/css'>\n    nav {\n        margin-left: " + maxMargin + "px !important;\n    }\n    aside.movie {\n        margin-right: " + maxMargin + "px !important;\n    }\n</style>");
      return rule.appendTo('head');
    };
    setMaxWidth();
    return $(window).bind('resize', setMaxWidth);
  });

}).call(this);
