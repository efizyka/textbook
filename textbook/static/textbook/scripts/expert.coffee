$ ->
    $('.props_audience_.expert')
        .wrap('<div class="expert"></div>')
    $('.props_audience_.expert').each( ->
        id = uuid()
        $(@)
            .attr('id', id)
            .before('<header class="collapsed" data-toggle="collapse" data-target="#'+id+'"><h1>Chcesz wiedzieć więcej?</h1></header>')
    )
    $('.props_audience_.expert')
        .addClass('body')
        .removeClass('expert')

#    $('.expert > header')
#    .addClass('collapsed')
#    .tooltip
#        placement: 'auto'
#        title: 'Zobacz więcej'
#    .on 'show.bs.tooltip', ->
#        $(this).hasClass('collapsed')
#    .on 'shown.bs.tooltip', ->
#        $this = $(this)
#        $this.next().css
#            'margin-left': -$this.width() / 2

