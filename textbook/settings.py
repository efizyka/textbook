import appsettings

register = appsettings.register('textbook')


@register(main=True)
class Globals:
    root = '/www/textbook'