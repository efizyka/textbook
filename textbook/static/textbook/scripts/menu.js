$(function() {
    var $nav = $('#nav');
    $('button.nav').click(function() {
        $nav.toggleClass('open');
        var max = $(window).height() - $nav.offset().top;
        $nav.css('max-height', max);
        //$('body').toggleClass('push');
        return false;
    });
    $('html').click(function(e) {
        if ($(e.target).closest('.open').length == 0)
            $('.open').removeClass('open');
        //$('body').removeClass('push');
        return true;
    });
});